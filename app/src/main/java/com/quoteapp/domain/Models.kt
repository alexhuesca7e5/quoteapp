package com.quoteapp.domain

data class Quote (
    val author: String,
    val content: String
    )

data class Tag (
    val name: String
    )