package com.quoteapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.quoteapp.domain.Quote
import com.quoteapp.domain.Tag
import com.quoteapp.network.Network
import kotlinx.coroutines.launch

class ViewModel  : ViewModel() {

    private val _tags = MutableLiveData<List<Tag>>()

    val tags: LiveData<List<Tag>>
        get() = _tags

    private val _currentQuote = MutableLiveData<Quote>()
    val currentQuote: LiveData<Quote>
        get() = _currentQuote


    init {
        getQuotes()
    }

    private fun getQuotes() {
        viewModelScope.launch {
            try {
                _tags.value = Network.service.getAllTags()


            } catch (e: Exception) {
                _tags.value = ArrayList()
            }
        }
    }


    fun updateQuote(tag: Tag) {
        viewModelScope.launch {

            try {
                _currentQuote.value = Network.service.getRandomByTag(tag.name)


            } catch (e: Exception) {
                _currentQuote.value = Quote("","")
            }
        }
    }

}