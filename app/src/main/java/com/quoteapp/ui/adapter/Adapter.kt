package com.quoteapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.quoteapp.databinding.ItemViewBinding
import com.quoteapp.domain.Quote
import com.quoteapp.domain.Tag
import com.quoteapp.ui.ListFragmentDirections

class Adapter(private val list: List<Tag>, val onItemClicked: (Tag) -> Unit) :
    RecyclerView.Adapter<Adapter.ViewHolder>() {

    class ViewHolder(private val binding: ItemViewBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Tag) {
            binding.textViewName.text = item.name
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layout = ItemViewBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )

        return ViewHolder(layout)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list.get(position)
        holder.bind(item)


        // Assigns a [OnClickListener] to the button contained in the [ViewHolder]
        holder.itemView.setOnClickListener {
            onItemClicked(item)
            val action = ListFragmentDirections.actionListFragmentToDetailFragment()
            holder.itemView.findNavController().navigate(action)
        }
    }

}