package com.quoteapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.quoteapp.databinding.FragmentDetailBinding
import com.quoteapp.viewmodels.ViewModel

class DetailFragment : Fragment() {
    private val viewModel: ViewModel by activityViewModels()

    private var _binding: FragmentDetailBinding? = null

    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.currentQuote.observe(viewLifecycleOwner){ quote ->
            binding.authorTextView.text = quote.author
            binding.quoteTextView.text = quote.content
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}