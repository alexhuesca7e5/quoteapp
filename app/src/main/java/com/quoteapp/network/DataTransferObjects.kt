package com.quoteapp.network

import com.quoteapp.domain.Quote
import com.quoteapp.domain.Tag
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NetworkTag(
    val _id: String,
    val dateAdded: String,
    val dateModified: String,
    val name: String,
    val quoteCount: Int,
    val slug: String
)

@JsonClass(generateAdapter = true)
data class NetworkQuote(
    val _id: String,
    val author: String,
    val authorSlug: String,
    val content: String,
    val dateAdded: String,
    val dateModified: String,
    val length: Int,
    val tags: List<String>
)


@JvmName("asModelNetworkQuote")
fun List<NetworkQuote>.asModel():List<Quote> {
    return  this.map{
        Quote(
            author = it.author,
            content = it.content
        )
    }
}

@JvmName("asModelNetworkTag")
fun List<NetworkTag>.asModel():List<Tag> {
    return  this.map{
        Tag(
            name = it.name
        )
    }
}