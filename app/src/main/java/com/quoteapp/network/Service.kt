package com.quoteapp.network

import com.quoteapp.domain.Quote
import com.quoteapp.domain.Tag
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


interface Service {
    @GET("tags")
    suspend fun getAllTags():List<Tag>

    @GET("random")
    suspend fun getRandomByTag(@Query("tags")tag: String):Quote
}

object Network{
    private const val BASE_URL = "https://api.quotable.io/"

    private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    private val retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .baseUrl(BASE_URL)
        .build()

    val service: Service = retrofit.create(Service::class.java)
}